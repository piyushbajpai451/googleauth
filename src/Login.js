import React, { Component } from 'react';
import './App.css';
import  '@material-ui/core';


import GoogleLogin from 'react-google-login';

class Login extends Component {

  constructor(props){
    super(props);
    this.state={
    username:'',
    password:''
    }
   }

   GoogleSignin.signIn().then((user) => {
    console.log(user);
    this.setState({user: user});
    console.log(user.token+" token");
    const credential = {
        provider: 'google',
        token: user.accessToken,
        secret: user.id, //I do not know what to send in secret
    };            
    Authentication.googleLogin(credential);
})
    .catch((err) => {
       alert('WRONG SIGNIN'+ err);
    })
    .done();

  render() {
    const responseGoogle = (response) => {
      console.log(response);
    }

    return (
      <div className="App">
      <meta name="google-signin-client_id" content="YOUR_CLIENT_ID.apps.googleusercontent.com"></meta>
        <h1>LOGIN WITH GOOGLE</h1>

    
      <br />
      <br />


      <GoogleLogin
        clientId="63806756932-5kk957taoj1s8cd7vn6mfgf6m5tvhinc.apps.googleusercontent.com" 
        buttonText="LOGIN WITH GOOGLE"
        //onClick
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
      />

      </div>
    );
  }
}

export default Login;